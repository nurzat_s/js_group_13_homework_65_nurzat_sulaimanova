import { Component, ElementRef, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Movie } from '../shared/movie.model';
import { Subscription } from 'rxjs';
import { MovieService } from '../shared/movie.service';

@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.css']
})
export class MoviesComponent implements OnInit, OnDestroy{
  movies!: Movie[];
  moviesChangeSubscription!: Subscription;
  moviesFetchingSubscription!: Subscription;
  isFetching: boolean = false;

  constructor(private movieService: MovieService, private http: HttpClient) { }

  ngOnInit(): void {
    this.movies = this.movieService.getMovies();

    this.moviesChangeSubscription = this.movieService.moviesChange.subscribe((movies: Movie[]) => {
      this.movies = movies;
    })

    this.moviesFetchingSubscription = this.movieService.moviesFetching.subscribe((isFetching: boolean) => {
      this.isFetching = isFetching;
    });
    this.movieService.fetchMovies();
  }

  ngOnDestroy() {
    this.moviesChangeSubscription.unsubscribe();
    this.moviesFetchingSubscription.unsubscribe();
  }
}
