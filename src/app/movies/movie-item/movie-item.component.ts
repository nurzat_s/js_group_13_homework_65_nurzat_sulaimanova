import { Component, Input, OnInit } from '@angular/core';
import { Movie } from '../../shared/movie.model';
import { MovieService } from '../../shared/movie.service';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Params } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-movie-item',
  templateUrl: './movie-item.component.html',
  styleUrls: ['./movie-item.component.css']
})
export class MovieItemComponent implements OnInit{
  @Input() movies!: Movie;
  movie!: Movie;
  subscription!: Subscription;
  id!: string;


  constructor(private route: ActivatedRoute, private movieService: MovieService, private http: HttpClient) {}

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      const movieId = parseInt(params['id']);
      this.movie = this.movieService.getMovie(movieId);
    })
  }

  delete(id: string) {
    this.http.delete<Movie>(`https://plovo-e531e-default-rtdb.firebaseio.com/movies/${this.id}.json`)
      .subscribe();
    this.movieService.fetchMovies();
  }

}
