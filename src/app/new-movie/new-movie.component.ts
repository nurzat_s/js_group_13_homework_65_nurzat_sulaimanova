import { Component, ElementRef, Input, ViewChild } from '@angular/core';
import { Movie } from '../shared/movie.model';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-new-movie',
  templateUrl: './new-movie.component.html',
  styleUrls: ['./new-movie.component.css']
})
export class NewMovieComponent {
  @ViewChild('nameInput') nameInput!: ElementRef;

  constructor(private http: HttpClient) { }

  createMovie() {
    const name = this.nameInput.nativeElement.value;

    const body = {name};
    this.http.post('https://plovo-e531e-default-rtdb.firebaseio.com/movies.json', body).subscribe();
  }
}
