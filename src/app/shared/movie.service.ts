import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Movie } from './movie.model';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable()

export class MovieService {
  moviesChange = new Subject<Movie[]>();
  moviesFetching = new Subject<boolean>();
  id!: string;

  constructor(private http: HttpClient) { }

  private movies: Movie[] = [];

  fetchMovies() {
    this.moviesFetching.next(true);

    this.http.get<{[id: string]: Movie}>('https://plovo-e531e-default-rtdb.firebaseio.com/movies.json')
      .pipe(map(result => {
        if(result === null) {
          return [];
        }
        return Object.keys(result).map(id => {
          const movieData = result[id];
          return new Movie(id, movieData.name);
        });
      }))
      .subscribe(movies => {
        this.movies = movies;
        this.moviesChange.next(this.movies.slice());
        this.moviesFetching.next(false);
      }, error => {
        this.moviesFetching.next(false);
      });
  }

  getMovies() {
    return this.movies.slice();
  }

  addMovie(movie: Movie) {
    this.movies.push(movie);
    this.moviesChange.next(this.movies.slice());
  }

  getMovie(index: number) {
    return this.movies[index];
  }


}
